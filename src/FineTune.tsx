import React, { useEffect, useState } from 'react';
import { Button, Divider, Input, Space, Upload } from 'antd';
import 'antd/dist/antd.css';
import request from './request';

// const fileID = 'file-mDZdtU86e4QvwNYW8Qbqqbzc';
const fileID = 'file-EbcLh9WSnJ1nkRJVGiv67y5m';
const fineTuneID = 'ft-uxA08G32TWJKyftk9g8aaG1L';

function FineTune() {
  const [file, setFile] = useState<File>();
  const [prompt, setPrompt] = useState<string>('');
  const [answer, setAnswer] = useState<string>('');
  const [cancelTuneID, setCancelTuneID] = useState<string>('');

  const uploadFile = async () => {
    // const res = await openai.createFile(file, 'fine-tune');

    const formData = new FormData();
    formData.append('file', file);
    formData.append('purpose', 'fine-tune');

    await request('https://api.openai.com/v1/files', {
      method: 'POST',
      body: formData,
    });
  };

  const updatePrompt = (e) => {
    setPrompt(e.target.value);
  };

  const updateCancelTuneID = (e) => {
    setCancelTuneID(e.target.value);
  };

  const handleCompletion = async () => {
    setAnswer('...');
    const res = await request('https://api.openai.com/v1/completions', {
      method: 'POST',
      body: {
        prompt,
        model: `text-davinci-003`,
        temperature: 0.9,
        max_tokens: 1000,
        frequency_penalty: 0,
        presence_penalty: 0.6,
      },
    });
    setAnswer(res.choices[0].text);
  };

  const createFineTune = async () => {
    const res = await request('https://api.openai.com/v1/fine-tunes', {
      method: 'POST',
      body: {
        training_file: fileID,
        model: 'davinci',
      },
    });
  };

  const getFineTune = async () => {
    const res = await request(
      `https://api.openai.com/v1/fine-tunes/${fineTuneID}`,
      { method: 'GET' }
    );
  };

  const getFile = async () => {
    const res = await request(`https://api.openai.com/v1/files/${fileID}`, {
      method: 'GET',
    });
  };

  const getListModel = async () => {
    const res = await request('https://api.openai.com/v1/models', {
      method: 'GET',
    });
  };

  const onUploadChange = ({ file }) => {
    setFile(file);
  };

  const getFineTunes = async () => {
    const res = await request('https://api.openai.com/v1/fine-tunes', {
      method: 'GET',
    });
  };

  const cancelFineTune = async () => {
    const res = await request(
      `https://api.openai.com/v1/fine-tunes/${cancelTuneID}/cancel`,
      {
        method: 'POST',
      }
    );
  };

  useEffect(() => {}, []);

  return (
    <div className='App'>
      <Upload
        listType='picture-card'
        onChange={onUploadChange}
        beforeUpload={() => false}
        maxCount={1}
      >
        JSONL
      </Upload>
      <Space>
        <Button type='primary' onClick={uploadFile}>
          Upload File
        </Button>
        <Button type='primary' onClick={createFineTune}>
          Create Fine-tune
        </Button>
        <span>--</span>
        <Button type='primary' onClick={getFile}>
          Get File
        </Button>
        <Button type='primary' onClick={getFineTune}>
          Get Fine-tune
        </Button>
        <Button type='primary' onClick={getFineTunes}>
          Get List Fine-tune
        </Button>
      </Space>

      <Divider />
      <Space>
        <Input type='text' onChange={updateCancelTuneID} size={'middle'} />
        <Button type='primary' onClick={cancelFineTune}>
          Cancel Fine-tune
        </Button>
      </Space>

      <Divider />
      <Input.TextArea
        onChange={updatePrompt}
        size={'middle'}
        style={{ marginBottom: 20, height: 300 }}
      />
      <Space>
        <Button type='primary' onClick={handleCompletion}>
          Send prompt
        </Button>
        <Button type='primary' onClick={getListModel}>
          List model
        </Button>
      </Space>
      <Divider />
      <Space>{answer}</Space>
    </div>
  );
}

export default FineTune;
