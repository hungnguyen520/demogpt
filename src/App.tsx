import React, { useEffect, useRef, useState } from 'react';
import { Button, Divider, Form, Input, Radio, Space, Upload } from 'antd';
import request from './request';
import './app.scss';
import Title from 'antd/lib/typography/Title';
import { SyncOutlined } from '@ant-design/icons';

const defaultInput = {
  general_companyName: 'NFQ',
  general_purpose: 'Software outsourcing',
  general_description:
    'We are a Germany company, we hiring the talent of Vietnam and Germany to serve the best software services around the world',
  general_keywords: 'dev, php, nodejs, reactjs, german, vietnam',
  component_topic: '',
  component_author: 'NFQ',
  component_sentiment: 'serious',
  component_keywords: '',
  component_content: '',
  gpt_maxTokens: 1200,
};

function App() {
  const [form] = Form.useForm();
  const [prompt, setPrompt] = useState<string>('');
  const [answer, setAnswer] = useState<string>('');
  const [maxTokens, setMaxTokens] = useState<number>(500);
  const emptyIDs = useRef<string[]>([]);

  const onGetPrompt = () => {
    setPrompt('');
    form.validateFields().then((values) => {
      const emptyKeys = [];
      const maxTokens = values.gpt_maxTokens;
      if (maxTokens) {
        setMaxTokens(maxTokens);
      }
      let givenPrompt = '';
      let emptyPrompt = '';
      Object.keys(values).forEach((k) => {
        const v = values[k];
        if (k.indexOf('gpt_') < 0) {
          if (v) {
            givenPrompt += `"${k}": "${v}" \n`;
          } else {
            emptyPrompt += `"${k}": \n`;
            emptyKeys.push(k);
          }
        } else {
          setMaxTokens(v);
        }
      });
      emptyIDs.current = emptyKeys;
      setPrompt(
        `Given\n${givenPrompt}\nEmpty\n${emptyPrompt}\nComplete Empty base on Given line by line`
      );
    });
  };

  const fillTheForm = (result) => {
    const resultLines = result.replace(/^\s+|\s+$/g, '').split('\n');
    emptyIDs.current.forEach((componentId) => {
      resultLines.forEach((line) => {
        if (line.indexOf(componentId) > -1) {
          const v = line
            .split(':')[1]
            ?.replace(/^\s+|\s+$/g, '')
            .replace(/"/g, '');

          form.setFieldValue(componentId, v);

          const el: any = document.getElementById(componentId);
          el.classList.add('hightlight');
        }
      });
    });
  };

  const completePrompt = async () => {
    setAnswer('...');
    const res = await request('https://api.openai.com/v1/completions', {
      method: 'POST',
      body: {
        prompt,
        model: `text-davinci-003`,
        temperature: 0.8,
        max_tokens: maxTokens,
        frequency_penalty: 0,
        presence_penalty: 0,
        top_p: 1,
      },
    });
    const result = res.choices[0].text;
    setAnswer(result);
  };

  useEffect(() => {
    if (answer !== '...') {
      fillTheForm(answer);
    }
  }, [answer]);

  return (
    <div className='app'>
      <div className='panel'>
        <Title level={2}>General settings</Title>
        <Form
          form={form}
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          initialValues={defaultInput}
        >
          <Form.Item label='Company name' name='general_companyName'>
            <Input />
          </Form.Item>

          <Form.Item label='Purpose' name='general_purpose'>
            <Input />
          </Form.Item>

          <Form.Item label='Description' name='general_description'>
            <Input />
          </Form.Item>

          <Form.Item label='Keywords' name='general_keywords'>
            <Input />
          </Form.Item>

          <Divider />

          <Title level={3}>Component Info</Title>

          <Form.Item label='Topic' name='component_topic'>
            <Input />
          </Form.Item>
          <Form.Item label='Author' name='component_author'>
            <Input />
          </Form.Item>
          <Form.Item label='Sentiment' name='component_sentiment'>
            <Radio.Group>
              <Radio.Button value='fun'>Fun</Radio.Button>
              <Radio.Button value='factual'>Factual</Radio.Button>
              <Radio.Button value='serious'>Serious</Radio.Button>
              <Radio.Button value='citical'>Citical</Radio.Button>
            </Radio.Group>
          </Form.Item>
          <Form.Item label='Keywords' name='component_keywords'>
            <Input />
          </Form.Item>
          <Form.Item label='Content' name='component_content'>
            <Input.TextArea className='component-content' />
          </Form.Item>
          <Divider />

          <Title level={4}>GPT options</Title>
          <Form.Item label='Max length of tokens' name='gpt_maxTokens'>
            <Input type='number' style={{ width: 100 }} />
          </Form.Item>
        </Form>
      </div>
      <div className='panel'>
        <Title level={2}>ChatGPT</Title>
        {prompt ? (
          <Input.TextArea
            className='prompt'
            defaultValue={prompt}
            key='has-prompt'
            onChange={(e) => setPrompt(e.target.value)}
          />
        ) : (
          <Input.TextArea
            className='prompt'
            key='no-prompt'
            onChange={(e) => setPrompt(e.target.value)}
          />
        )}

        <Divider />
        <Space className='button-wrapper'>
          <Button type='primary' size='small' onClick={onGetPrompt}>
            Get prompt
          </Button>
          <Button type='primary' onClick={completePrompt} size='large'>
            GET ANSWER !!
          </Button>
        </Space>
        <Divider />
        <Title level={5}>Answer: </Title>
        {answer === '...' ? (
          <SyncOutlined spin />
        ) : (
          <pre className='answer'>{answer}</pre>
        )}
      </div>
      <div className='panel'>
        <Title level={2}> Estimated Bill</Title>
      </div>
    </div>
  );
}

export default App;
